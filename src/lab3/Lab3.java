/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lab3;

/**
 *
 * @author kelsey.pritsker676
 */
public class Lab3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        CalcGUI view = new CalcGUI();
        CalcModel model = new CalcModel();
        
        CalcController controller = new CalcController(view,model);
        
        view.setVisible(true);
    }
}
